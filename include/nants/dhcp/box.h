/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef NANTS_DHCP_BOX_H
#define NANTS_DHCP_BOX_H

#include <chrono>
#include <ranges>
#include <span>
#include <vector>
#include <cstddef>
#include <cstdint>

#include <nants/dhcp/offer.h>
#include <nants/dhcp/message/basic.h>
#include <nants/dhcp/message/type.h>
#include <nants/dhcp/message/opcode.h>

#include <nants/dhcp/message/option/code.h>
#include <nants/dhcp/message/option/dhcp_message_type.h>
#include <nants/dhcp/message/option/parameter_request_list.h>
#include <nants/dhcp/message/option/server_identifier.h>
#include <nants/dhcp/message/option/requested_ip_address.h>

namespace nants::dhcp
{
	struct offer;

	template<class UdpSocket, class Clock> class box
	{
	public:
		using socket_type = UdpSocket;
		using clock_type = Clock;

		template<std::integral T, std::size_t N> explicit box(const std::span<T, N> hw_addr) noexcept :
			m_hw_addr(begin(hw_addr), end(hw_addr))
		{

		}

		std::vector<offer> discover(const std::chrono::milliseconds period = std::chrono::milliseconds{1000}, const std::vector<message::option::code> parameters = std::vector<message::option::code>{message::option::code::subnet_mask, message::option::code::router, message::option::code::domain_name_server}) noexcept
		{
			auto m{create_discover(parameters)};

			socket_type::send(m.encode());

			std::vector<offer> offers;
			const auto discover_started{clock_type::now()};

			// while (clock_type::now() < discover_started + period)
			{
				const auto received{socket_type::recv(m.buffer())};

				if (received > message::minimum_length && m.template get_option<message::option::code::dhcp_message_type>() == message::type::offer)
				{
					offers.emplace_back(extract_offer(m));
				}
			}

			return offers;
		}

		message::message request(const offer& of) noexcept
		{
			auto m{create_request(of)};

			socket_type::send(m.encode());

			const auto received{socket_type::recv(m.buffer())};

			return m;
		}

	private:
		message::message create_message() noexcept
		{
			message::message m;
			m.set_op(message::opcode::request);
			m.set_hardware_address(std::span{m_hw_addr});
			m.set_xid(std::span{m_hw_addr}.last(4));

			return m;
		}
		message::message create_discover(const std::vector<message::option::code>& parameters) noexcept
		{
			auto m{create_message()};
			m.template set_option<message::option::code::dhcp_message_type>(message::type::discover);
			m.template set_option<message::option::code::parameter_request_list>(parameters);

			return m;
		}

		message::message create_request(const offer& of) noexcept
		{
			auto m{create_message()};
			m.template set_option<message::option::code::dhcp_message_type>(message::type::request);
			m.template set_option<message::option::code::requested_ip_address>(std::span{of.client_address});
			m.template set_option<message::option::code::server_identifier>(std::span{of.server_address});

			return m;
		}

		static offer extract_offer(const message::message& m) noexcept
		{
			assert(m.template get_option<message::option::code::dhcp_message_type>() == message::type::offer);

			offer of;
			const auto& siaddr{m.server_address()};
			const auto& yiaddr{m.your_address()};

			std::copy(begin(siaddr), end(siaddr), begin(of.server_address));
			std::copy(begin(yiaddr), end(yiaddr), begin(of.client_address));

			return of;
		}

		std::vector<std::uint8_t> m_hw_addr;
	};
}

#endif // NANTS_DHCP_BOX_H
