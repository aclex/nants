/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef NANTS_DHCP_MESSAGE_BASIC_H
#define NANTS_DHCP_MESSAGE_BASIC_H

#include <algorithm>
#include <cassert>

#include <nants/dhcp/message/opcode.h>

#include <nants/dhcp/message/option/code.h>
#include <nants/dhcp/message/option/item.h>

namespace nants::dhcp::message
{
	constexpr inline std::size_t minimum_length{241};
	constexpr inline std::size_t maximum_length{576};

	class message
	{
	public:
		message() noexcept :
			message(std::span<std::uint8_t>{}, std::vector<std::uint8_t>(maximum_length))
		{
			initialize();
		}

		template<std::size_t N> message(const std::span<std::uint8_t, N> data, std::vector<std::uint8_t>&& buffer = {}) noexcept :
			m_buffer(std::move(buffer)),
			m_data(m_buffer.empty() ? data : std::span{m_buffer}),
			m_opcode(m_data.subspan(0, 1)),
			m_htype(m_data.subspan(1, 1)),
			m_hlen(m_data.subspan(2, 1)),
			m_hops(m_data.subspan(3, 1)),
			m_xid(m_data.subspan(4, 4)),
			m_secs(m_data.subspan(8, 2)),
			m_flags(m_data.subspan(10, 2)),
			m_ciaddr(m_data.subspan(12, 4)),
			m_yiaddr(m_data.subspan(16, 4)),
			m_siaddr(m_data.subspan(20, 4)),
			m_giaddr(m_data.subspan(24, 4)),
			m_chaddr(m_data.subspan(28, 16)),
			m_sname(m_data.subspan(44, 64)),
			m_file(m_data.subspan(108, 128)),
			m_magic_cookie(m_data.subspan(236, 4)),
			m_options(m_data.subspan(240, 336)),
			m_size(maximum_length)
		{

		}

		opcode op() const noexcept
		{
			return opcode{m_opcode[0]};
		}

		void set_op(const opcode p) noexcept
		{
			m_opcode[0] = static_cast<std::uint8_t>(p);
		}

		std::span<std::uint8_t> xid() const noexcept
		{
			return m_xid;
		}

		template<std::integral T, std::size_t N> void set_xid(const std::span<T, N> p) noexcept
		{
			std::copy(begin(p), end(p), begin(m_xid));
		}

		std::span<std::uint8_t> hardware_address() const noexcept
		{
			return m_chaddr.first(m_hlen[0]);
		}

		template<std::integral T, std::size_t N> void set_hardware_address(const std::span<T, N> addr) noexcept
		{
			static_assert(sizeof(T) == sizeof(std::uint8_t), "Octet-based type is required.");
			std::copy(begin(addr), end(addr), begin(m_chaddr));
			m_hlen[0] = static_cast<std::uint8_t>(addr.size());
		}

		std::span<std::uint8_t> client_address() const noexcept
		{
			return m_ciaddr;
		}

		template<std::integral T, std::size_t N> void set_client_address(const std::span<T, N> addr) noexcept
		{
			std::copy(begin(addr), end(addr), begin(m_ciaddr));
		}

		std::span<std::uint8_t> server_address() const noexcept
		{
			return m_siaddr;
		}

		template<std::integral T, std::size_t N> void set_server_address(const std::span<T, N> addr) noexcept
		{
			std::copy(begin(addr), end(addr), begin(m_siaddr));
		}

		std::span<std::uint8_t> your_address() const noexcept
		{
			return m_yiaddr;
		}

		template<std::integral T, std::size_t N> void set_your_address(const std::span<T, N> addr) noexcept
		{
			std::copy(begin(addr), end(addr), begin(m_yiaddr));
		}

		template<option::code c> option::item<c> get_option() const noexcept
		{
			return option::item<c>{m_options};
		}

		template<option::code c, typename T> void set_option(T&& t) noexcept
		{
			option::item<c> op{m_options};
			op = std::forward<T>(t);
		}

		std::span<std::uint8_t> encode() const noexcept
		{
			return m_data.first(m_size);
		}

		std::size_t size() const noexcept
		{
			return m_size;
		}

		std::span<std::uint8_t> buffer() noexcept
		{
			return m_data;
		}

	private:
		void initialize() noexcept
		{
			m_htype[0] = 1; // MAC address, pretty much constant

			m_xid[0] = 0xba;
			m_xid[1] = 0x18;
			m_xid[2] = 0x32;
			m_xid[3] = 0x0f;

			m_magic_cookie[0] = 0x63;
			m_magic_cookie[1] = 0x82;
			m_magic_cookie[2] = 0x53;
			m_magic_cookie[3] = 0x63;

			m_options[0] = 0xff;
			
			m_size = end(m_options) - begin(m_data);
		}

		std::vector<std::uint8_t> m_buffer;

		const std::span<std::uint8_t> m_data;
		const std::span<std::uint8_t> m_opcode;
		const std::span<std::uint8_t> m_htype;
		const std::span<std::uint8_t> m_hlen;
		const std::span<std::uint8_t> m_hops;
		const std::span<std::uint8_t> m_xid;
		const std::span<std::uint8_t> m_secs;
		const std::span<std::uint8_t> m_flags;
		const std::span<std::uint8_t> m_ciaddr;
		const std::span<std::uint8_t> m_yiaddr;
		const std::span<std::uint8_t> m_siaddr;
		const std::span<std::uint8_t> m_giaddr;
		const std::span<std::uint8_t> m_chaddr;
		const std::span<std::uint8_t> m_sname;
		const std::span<std::uint8_t> m_file;
		const std::span<std::uint8_t> m_magic_cookie;
		const std::span<std::uint8_t> m_options;

		std::size_t m_size;
	};
}

#endif // NANTS_DHCP_MESSAGE_BASIC_H
