/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef NANTS_DHCP_MESSAGE_OPTION_ADDRESS_H
#define NANTS_DHCP_MESSAGE_OPTION_ADDRESS_H

#include <span>
#include <type_traits>
#include <cstdint>
#include <cstddef>

#include <nants/dhcp/message/option/internals.h>

namespace nants::dhcp::message::option
{
	template<code c, std::size_t s> class address : private internals
	{
	public:
		using internals::internals;

		operator std::span<std::uint8_t>() const
		{
			const auto it{find(s_code)};

			if (valid(it) && !it.done())
			{
				return subspan(it, s_size);
			}

			throw std::invalid_argument("Option not found in the message.");
		}

		template<std::integral T, std::size_t N> address& operator=(const std::span<T, N> v) noexcept
		{
			auto it{find(s_code)};

			if (!valid(it))
				return *this;

			if (it.done())
				it = insert(s_code, s_size);

			const std::span<std::uint8_t>::iterator sit{it};
			std::copy(std::begin(v), std::end(v), sit + 2);

			return *this;
		}

	private:
		static constexpr inline auto s_code{c};
		static constexpr inline auto s_size{s};
	};
}

#endif // NANTS_DHCP_MESSAGE_OPTION_ADDRESS_H
