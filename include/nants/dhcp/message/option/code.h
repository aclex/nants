/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef NANTS_DHCP_MESSAGE_OPTION_CODE_H
#define NANTS_DHCP_MESSAGE_OPTION_CODE_H

namespace nants::dhcp::message::option
{
	enum class code : unsigned char
	{
		pad = 0,
		subnet_mask = 1,
		router = 3,
		domain_name_server = 6,
		host_name = 12,
		domain_name = 15,
		network_time_protocol_servers = 42,
		requested_ip_address = 50,
		ip_address_lease_time = 51,
		dhcp_message_type = 53,
		server_identifier = 54,
		parameter_request_list = 55,
		message = 56,
		maximum_dhcp_message_size = 57,
		client_identifier = 61,
		end = 255
	};
}

#endif // NANTS_DHCP_MESSAGE_OPTION_CODE_H
