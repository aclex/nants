/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef NANTS_DHCP_MESSAGE_OPTION_DOMAIN_NAME_SERVER_H
#define NANTS_DHCP_MESSAGE_OPTION_DOMAIN_NAME_SERVER_H

#include <algorithm>
#include <vector>
#include <cstdint>

#include <nants/dhcp/message/option/item.h>
#include <nants/dhcp/message/option/internals.h>

namespace nants::dhcp::message::option
{
	template<> class item<code::domain_name_server> : private internals
	{
	public:
		using internals::internals;

		operator std::vector<std::span<uint8_t>>() const;
		template<std::integral T, std::size_t N> item& operator=(const std::vector<std::span<T, N>>& v) noexcept
		{
			auto it{find(code::domain_name_server)};
			const auto size{v.size() * 4};

			if (!valid(it))
				return *this;

			if (it.done())
				it = insert(code::domain_name_server, size);

			const auto sit{it.unwrap()};
			for (auto nit = sit + 2; nit < sit + 2 + size; nit += 4)
			{
				const auto addr{v[(nit - sit) / 4]};
				std::copy(std::begin(addr), std::end(addr), nit);
			}

			return *this;
		}
	};
}

#endif // NANTS_DHCP_MESSAGE_OPTION_DOMAIN_NAME_SERVER_H
