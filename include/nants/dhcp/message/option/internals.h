/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef NANTS_DHCP_MESSAGE_OPTION_INTERNALS_H
#define NANTS_DHCP_MESSAGE_OPTION_INTERNALS_H

#include <nants/dhcp/message/option/code.h>
#include <nants/dhcp/message/option/iterator.h>

namespace nants::dhcp::message::option
{
	class internals
	{
	public:
		explicit internals(const std::span<std::uint8_t> options) noexcept;
		iterator find(const code oc) const noexcept;
		iterator find(const code oc, const iterator hint) const noexcept;
		iterator find_options_end() const noexcept;
		iterator find_options_end(const iterator hint) const noexcept;
		iterator insert(const code oc, const std::size_t s) noexcept;
		iterator insert(const code oc, const std::size_t s, const iterator hint) noexcept;
		iterator resize(const code oc, const std::size_t s) noexcept;
		iterator resize(const code oc, const std::size_t s, const iterator hint) noexcept;
		iterator erase(const code oc) noexcept;
		iterator maybe_mark_end(const iterator it) noexcept;
		bool valid(const iterator it) const noexcept;
		std::span<std::uint8_t> subspan(const iterator it, const std::size_t size) const noexcept;
		std::span<std::uint8_t> subspan(const std::span<std::uint8_t>::iterator it, const std::size_t size) const noexcept;

	private:
		const std::span<std::uint8_t> m_options;
	};
}

#endif // NANTS_DHCP_MESSAGE_OPTION_INTERNALS_H
