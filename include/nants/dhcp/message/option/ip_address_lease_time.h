/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef NANTS_DHCP_MESSAGE_OPTION_IP_ADDRESS_LEASE_TIME_H
#define NANTS_DHCP_MESSAGE_OPTION_IP_ADDRESS_LEASE_TIME_H

#include <chrono>

#include <nants/dhcp/message/option/item.h>
#include <nants/dhcp/message/option/internals.h>

#include <nants/dhcp/message/type.h>

namespace nants::dhcp::message::option
{
	template<> class item<code::ip_address_lease_time> : private internals
	{
	public:
		using internals::internals;

		operator std::chrono::seconds() const;
		item& operator=(const std::chrono::seconds s) noexcept;
	};
}

#endif // NANTS_DHCP_MESSAGE_OPTION_IP_ADDRESS_LEASE_TIME_H
