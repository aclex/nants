/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef NANTS_DHCP_MESSAGE_OPTION_ITERATOR_H
#define NANTS_DHCP_MESSAGE_OPTION_ITERATOR_H

#include <span>
#include <cstdint>

namespace nants::dhcp::message::option
{
	class iterator
	{
	public:
		iterator(const std::span<std::uint8_t>::iterator it) noexcept;
		iterator operator++() noexcept;
		iterator operator++(int) noexcept;
		typename std::span<std::uint8_t>::iterator operator->() const noexcept {return m_it;}
		typename std::span<std::uint8_t>::iterator operator*() const noexcept {return m_it;}
		operator typename std::span<std::uint8_t>::iterator() const noexcept {return m_it;}
		std::span<std::uint8_t>::iterator unwrap() const noexcept {return m_it;}
		bool done() const noexcept;
		bool operator==(const std::span<std::uint8_t>::iterator it) const noexcept {return m_it == it;}
		bool operator!=(const std::span<std::uint8_t>::iterator it) const noexcept {return !operator==(it);}

	private:
		std::uint8_t current_option_size() const noexcept;
		bool current_option_is_end() const noexcept;

		typename std::span<std::uint8_t>::iterator m_it;
	};
}

#endif // NANTS_DHCP_MESSAGE_OPTION_ITERATOR_H
