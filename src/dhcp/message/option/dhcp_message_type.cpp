/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "nants/dhcp/message/option/dhcp_message_type.h"

#include <stdexcept>

using namespace std;

using namespace nants::dhcp::message::option;

item<code::dhcp_message_type>::operator type() const
{
	const auto it{find(code::dhcp_message_type)};

	if (valid(it) && !it.done())
	{
		const span<uint8_t>::iterator sit{it};
		return type{static_cast<uint8_t>(*(sit + 2))};
	}

	throw invalid_argument("Option not found in the message.");
}

item<code::dhcp_message_type>& item<code::dhcp_message_type>::operator=(const type t) noexcept
{
	auto it{find(code::dhcp_message_type)};

	if (!valid(it))
		return *this;

	if (it.done())
		it = insert(code::dhcp_message_type, 1);

	const span<uint8_t>::iterator sit{it};
	*(sit + 2) = static_cast<uint8_t>(t);

	return *this;
}
