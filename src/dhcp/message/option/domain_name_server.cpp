/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "nants/dhcp/message/option/domain_name_server.h"

#include <stdexcept>

using namespace std;

using namespace nants::dhcp::message::option;

item<code::domain_name_server>::operator vector<span<uint8_t>>() const
{
	const auto it{find(code::domain_name_server)};

	if (valid(it) && !it.done())
	{
		const auto sit{it.unwrap()};
		const auto size{*(sit + 1)};
		vector<span<uint8_t>> result(size / 4);
		for (auto nit = sit + 2; nit < sit + 2 + size; nit += 4)
		{
			result.push_back(subspan(nit, 4));
		}

		return result;
	}

	throw invalid_argument("Option not found in the message.");
}
