/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "nants/dhcp/message/option/internals.h"

#include <algorithm>
#include <limits>
#include <cassert>

using namespace std;

using namespace nants::dhcp::message;
using namespace nants::dhcp::message::option;

internals::internals(const span<uint8_t> options) noexcept :
	m_options(options)
{

}

option::iterator internals::find(const code oc) const noexcept
{
	return find(oc, begin(m_options));
}

option::iterator internals::find(const code oc, const option::iterator hint) const noexcept
{
	for (option::iterator it = hint; it != end(m_options); ++it)
	{
		if (*it.unwrap() == static_cast<uint8_t>(code::end) ||
			*it.unwrap() == static_cast<uint8_t>(oc))
		{
			return it;
		}
	}

	return end(m_options);
}

option::iterator internals::find_options_end() const noexcept
{
	return find_options_end(begin(m_options));
}

option::iterator internals::find_options_end(const option::iterator hint) const noexcept
{
	return static_cast<span<uint8_t>::iterator>(find(code::end, hint));
}

option::iterator internals::insert(const code oc, const std::size_t s) noexcept
{
	return insert(oc, s, find(oc));
}

option::iterator internals::insert(const code oc, const std::size_t s, const option::iterator hint) noexcept
{
	auto it{hint};

	if (it == end(m_options)) // no room
		return it;

	if (!it.done())
		return resize(oc, s);

	span<uint8_t>::iterator sit{it};

	*sit = static_cast<uint8_t>(oc);
	assert(s < numeric_limits<uint8_t>::max());
	*(sit + 1) = static_cast<uint8_t>(s);

	maybe_mark_end(it);

	return it;
}

option::iterator internals::resize(const code oc, const std::size_t s) noexcept
{
	return resize(oc, s, find(oc));
}

option::iterator internals::resize(const code oc, const std::size_t s, option::iterator hint) noexcept
{
	auto it{hint};

	if (it == end(m_options)) // no room
		return it;

	if (it.done())
		return end(m_options);

	assert(s < numeric_limits<uint8_t>::max());

	const span<uint8_t>::iterator sit{it};

	const auto prev_size{*(sit + 1)};
	if (prev_size == static_cast<uint8_t>(s))
		return it;

	const span<uint8_t>::iterator options_end_sit{find_options_end(it)};

	if (prev_size < static_cast<uint8_t>(s))
		move_backward(sit + prev_size + 2, options_end_sit + 1, sit + s + 2);
	else
		move(sit + prev_size + 2, options_end_sit + 1, sit + s + 2);

	*(sit + 1) = static_cast<uint8_t>(s);

	return it;
}

option::iterator internals::erase(const code oc) noexcept
{
	const auto it{find(oc)};

	if (it == end(m_options) || it.done())
		return end(m_options);

	const span<uint8_t>::iterator sit{it};
	const span<uint8_t>::iterator options_end_sit{find_options_end(it)};

	const auto size{*(sit + 1)};

	move(sit + size + 1, options_end_sit + 1, sit);

	fill(options_end_sit + 1 - size, end(m_options), 0);

	return it;
}

option::iterator internals::maybe_mark_end(const option::iterator hint) noexcept
{
	auto it{hint};

	++it;

	if (it == end(m_options))
		return end(m_options);

	if (it.done())
	{
		*it.unwrap() = static_cast<uint8_t>(code::end);
		return it;	
	}

	return hint;
}

bool internals::valid(const option::iterator it) const noexcept
{
	return it != end(m_options);
}

span<uint8_t> internals::subspan(const option::iterator it, const size_t size) const noexcept
{
	return subspan(it.unwrap(), size);
}

span<uint8_t> internals::subspan(const span<uint8_t>::iterator it, const size_t size) const noexcept
{
	return m_options.subspan(it + 2 - begin(m_options), size);
}
