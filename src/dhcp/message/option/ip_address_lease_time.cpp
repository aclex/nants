/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "nants/dhcp/message/option/ip_address_lease_time.h"

#include <stdexcept>

using namespace std;
using namespace std::chrono;

using namespace nants::dhcp::message::option;

item<code::ip_address_lease_time>::operator seconds() const
{
	const auto it{find(code::ip_address_lease_time)};

	if (valid(it) && !it.done())
	{
		const span<uint8_t>::iterator sit{it};
		seconds result
		{
			*(sit + 2) << 24 | *(sit + 3) << 16 | *(sit + 4) << 8 | *(sit + 5)
		};

		return result;
	}

	throw invalid_argument("Option not found in the message.");
}

item<code::ip_address_lease_time>& item<code::ip_address_lease_time>::operator=(const seconds s) noexcept
{
	auto it{find(code::ip_address_lease_time)};

	if (!valid(it))
		return *this;

	if (it.done())
		it = insert(code::ip_address_lease_time, 4);

	const span<uint8_t>::iterator sit{it};
	const auto v{s.count()};

	*(sit + 2) = static_cast<uint8_t>((v >> 24) & 0xff);
	*(sit + 3) = static_cast<uint8_t>((v >> 16) & 0xff);
	*(sit + 4) = static_cast<uint8_t>((v >> 8) & 0xff);
	*(sit + 5) = static_cast<uint8_t>(v & 0xff);

	return *this;
}
