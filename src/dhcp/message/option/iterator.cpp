/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "nants/dhcp/message/option/iterator.h"

#include "nants/dhcp/message/option/code.h"

using namespace std;

using namespace nants::dhcp::message;

option::iterator::iterator(const span<uint8_t>::iterator it) noexcept :
	m_it(it)
{

}

option::iterator option::iterator::operator++() noexcept
{
	if (*m_it == static_cast<uint8_t>(code::pad) || *m_it == static_cast<uint8_t>(code::end)) // fixed length options
	{
		++m_it;
	}
	else // variable length options
	{
		const auto sz{current_option_size()};
		m_it += sz + 2;
	}

	return *this;
}

option::iterator option::iterator::operator++(int) noexcept
{
	auto result{*this};

	++(*this);

	return result;
}

bool option::iterator::done() const noexcept
{
	return current_option_size() == 0;
}

uint8_t option::iterator::current_option_size() const noexcept
{
	return *(m_it + 1);
}

bool option::iterator::current_option_is_end() const noexcept
{
	return *(m_it) == static_cast<uint8_t>(code::end);
}
