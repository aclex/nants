/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "nants/dhcp/message/option/parameter_request_list.h"

#include <stdexcept>

using namespace std;

using namespace nants::dhcp::message::option;

item<code::parameter_request_list>::operator vector<code>() const
{
	const auto it{find(code::parameter_request_list)};

	if (valid(it) && !it.done())
	{
		const auto sit{it.unwrap()};
		const auto size{*(sit + 1)};
		vector<code> result(size);
		transform(sit + 2, sit + 2 + size, begin(result), [](const uint8_t v){return code{v};});

		return result;
	}

	throw invalid_argument("Option not found in the message.");
}

item<code::parameter_request_list>& item<code::parameter_request_list>::operator=(const vector<code>& v) noexcept
{
	auto it{find(code::parameter_request_list)};

	if (!valid(it))
		return *this;

	if (it.done())
		it = insert(code::parameter_request_list, v.size());

	const auto sit{it.unwrap()};
	transform(begin(v), end(v), sit + 2, [](const code c){return static_cast<uint8_t>(c);});

	return *this;
}
