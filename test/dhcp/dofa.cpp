/*
	nants - micro C++ library of core network services
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>
#include <chrono>

#include <nants/dhcp/box.h>

#include <nants/dhcp/message/option/subnet_mask.h>
#include <nants/dhcp/message/option/router.h>
#include <nants/dhcp/message/option/ip_address_lease_time.h>
#include <nants/dhcp/message/option/domain_name_server.h>

#include "message_examples.h"

using namespace std;
using namespace std::chrono;

using namespace nants;
using namespace nants::dhcp;

namespace
{
	const array<uint8_t, 6> hw_addr{0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};

	class test_socket
	{
	public:
		static void send(const span<uint8_t> p) noexcept
		{
			switch (s_state)
			{
			case message::type::discover:
				check_discover(p);
				s_state = dhcp::message::type::offer;
				break;

			case message::type::request:
				check_request(p);
				s_state = dhcp::message::type::ack;
				break;

			default:
				break;
			}
		}

		static size_t recv(const span<uint8_t> p) noexcept
		{
			switch (s_state)
			{
			case message::type::offer:
				push_offer(p);
				s_state = dhcp::message::type::request;
				break;

			case message::type::ack:
				push_ack(p);
				s_state = dhcp::message::type::discover;
				break;

			default:
				break;
			}

			return p.size();
		}

	private:
		static void push_offer(const span<uint8_t> p) noexcept
		{
			dhcp::message::message m{p};
			m.set_op(message::opcode::reply);
			m.set_hardware_address(span{hw_addr});
			m.set_server_address(span{s_server_addr});
			m.set_your_address(span{s_client_addr});
			m.set_xid(span{hw_addr}.last(4));
			m.set_option<message::option::code::dhcp_message_type>(message::type::offer);
			m.set_option<message::option::code::subnet_mask>(span{s_subnet_mask});
			m.set_option<message::option::code::router>(span{s_server_addr});
			m.set_option<message::option::code::ip_address_lease_time>(700s);
			m.set_option<message::option::code::domain_name_server>(vector{span{s_dns1_addr}, span{s_dns2_addr}});
		}

		static void push_ack(const span<uint8_t> p) noexcept
		{
			dhcp::message::message m{p};
			m.set_op(message::opcode::reply);
			m.set_hardware_address(span{hw_addr});
			m.set_server_address(span{s_server_addr});
			m.set_your_address(span{s_client_addr});
			m.set_xid(span{hw_addr}.last(4));
			m.set_option<message::option::code::dhcp_message_type>(message::type::ack);
			m.set_option<message::option::code::subnet_mask>(span{s_subnet_mask});
			m.set_option<message::option::code::router>(span{s_server_addr});
			m.set_option<message::option::code::ip_address_lease_time>(700s);
			m.set_option<message::option::code::domain_name_server>(vector{span{s_dns1_addr}, span{s_dns2_addr}});
		}

		static void check_discover(const span<uint8_t> p) noexcept
		{
			dhcp::message::message m{p};
			const auto& pkt_xid{m.xid()};
			copy(begin(pkt_xid), end(pkt_xid), begin(s_xid));

			const auto& expected{test::dhcp::message::example::discover};

			if (size(p) != size(expected))
			{
				cerr << "DISCOVER message size doesn't match expected size." << endl;
				terminate();
			}

			for (size_t i = 0; i < size(p) && i < size(expected); ++i)
			{
				if (p[i] != expected[i])
				{
					cerr << "DISCOVER message mismatch at index " << i << '.' << endl;
					terminate();
				}
			}
		}

		static void check_request(const span<uint8_t> p) noexcept
		{
			const auto& expected{test::dhcp::message::example::request};

			if (size(p) != size(expected))
			{
				cerr << "REQUEST message size doesn't match expected size." << endl;
				terminate();
			}

			for (size_t i = 0; i < size(p) && i < size(expected); ++i)
			{
				if (p[i] != expected[i])
				{
					cerr << "REQUEST message mismatch at index " << i << '.' << endl;
					terminate();
				}
			}
		}

		static message::type inline s_state{message::type::discover};
		static inline array<uint8_t, 4> s_xid;
		constexpr static inline array<uint8_t, 4> s_client_addr{192, 168, 1, 58};
		constexpr static inline array<uint8_t, 4> s_server_addr{192, 168, 1, 1};
		constexpr static inline array<uint8_t, 4> s_subnet_mask{255, 255, 255, 0};
		constexpr static inline array<uint8_t, 4> s_dns1_addr{4, 4, 5, 5};
		constexpr static inline array<uint8_t, 4> s_dns2_addr{1, 2, 3, 4};
	};
}

int main(int, char**)
{
	auto t{span{hw_addr}};
	dhcp::box<test_socket, system_clock> dhcp_box(t);

	vector<dhcp::offer> offers;
	while (offers.empty())
	{
		offers = dhcp_box.discover();
	}
	const auto& dhcp_ack{dhcp_box.request(offers.front())};
	return 0;
}
